$(document).ready(function () {
    $('#new-conversation-submit').click(function () {
        let user_id = $('#current-user').val();
        let conversation_title = $('#new-conversation-title').val();
        let message_text = $('#new-conversation-message').val();
        if (message_text) {
            createConversationAjax(conversation_title, user_id, message_text);
        } else {
            alert('Введите текст сообщения');
        }
    });
    $('body').on('click', '.conversation-delete-button', function () {
        let conversation_id = $(this).attr('data-id');
        if (confirm('Удалить?')) {
            deleteConversationAjax(conversation_id);
        }
    });
    $('body').on('focusout', '.conversation-title-input', function () {
        let conversation_id = $(this).attr('data-id');
        $(this).hide();
        $('#conversation-title-' + conversation_id).text($(this).val()).show(300);
        updateConversationAjax(conversation_id, $(this).val());
    });
    $('body').on('click', '.message-submit', function () {
        let conversation_id = $(this).attr('data-conversation_id');
        let message_text = $('#new-message-' + conversation_id).val();
        if (message_text) {
            sendMessageAjax(message_text, conversation_id);
        } else {
            alert('Введите текст сообщения');
        }
    });
    $('body').on('click', '.follow-button-block', function () {
        let post_id = $(this).attr('data-post_id');
        let type = $(this).attr('data-type');
        let user_id = $('#current-user').val();
        updateFollowerAjax(post_id, user_id, type);
    });
    $('body').on('click', '.followers-dialog li', function () {
        let post_id = $(this).attr('data-post_id');
        let user_id = $(this).attr('data-user_id');
        let type = $(this).attr('data-type');
        updateFollowerAjax(post_id, user_id, type);
    });
});

function ajaxErrorsHandling(jqxhr, status, errorMsg){
    $('.loading').hide();
    if(jqxhr.status == 401){
        $(location).attr('href', '/login/')
    }
}
function ajaxSetup() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
}

function createConversationAjax(conversation_title, user_id, message_text) {
    $('.loading').show();
    ajaxSetup();
    $.ajax({
        url     : "/createConversationAjax/",
        dataType: "json",
        data    : {
            conversation_title: conversation_title,
            user_id           : user_id,
            message_text      : message_text
        },
        success : function (data) {
            $('.loading').hide();
            $('#new-conversation-title').val('');
            $('#new-conversation-message').val('');
            let users_string = '';
            for (var i = 0; i < data.success.users.length; i++) {
                users_string = users_string + '<li data-name="' + data.success.users[i].name + '" ' +
                    'data-conversation_id="' + data.success.conversation.id + '">' + data.success.users[i].name + '</li>'
            }
            let followers_string = '';
            for (var i = 0; i < data.success.users.length; i++) {
                if (data.success.users[i].id != $('#current-user').val) {
                    followers_string = followers_string +
                        '<li data-user_id="' + data.success.users[i].id + '" data-post_id="' + data.success.conversation.id + '" data-type="conversation"' +
                        '    class="follower-list-item-' + data.success.users[i].id + '">' +
                        data.success.users[i].name +
                        '  <i class="fa fa-plus-square-o toggle-follower" aria-hidden="true"></i>' +
                        '</li>';
                }
            }
            $('.conversations-block').prepend('' +
                '<div class="panel panel-default conversations-item"  id="conversations-item-' + data.success.conversation.id + '">\n' +
                '  <div class="panel-heading">\n' +
                '    <h3>\n' +
                '      <a href="/conversations/' + data.success.conversation.id + '" class="' + data.success.conversation.title_class + '"\n' +
                '          id="conversation-title-' + data.success.conversation.id + '">\n' +
                data.success.conversation.title +
                '      </a>\n' +
                '      <input type="text" style="display: none" id="conversation-title-input-' + data.success.conversation.id + '"\n' +
                '          class="conversation-title-input" value="' + data.success.conversation.title + '"\n' +
                '          data-id="' + data.success.conversation.id + '">\n' +
                '      <div class="float-right conversation-header-button conversation-delete-button"\n' +
                '          data-id="' + data.success.conversation.id + '">\n' +
                '        <i class="fa fa-trash-o" aria-hidden="true"></i>\n' +
                '      </div>\n' +
                '      <div class="float-right conversation-header-button conversation-edit-title-button"\n' +
                '          data-id="' + data.success.conversation.id + '">\n' +
                '        <i class="fa fa-pencil" aria-hidden="true"></i>\n' +
                '      </div>\n' +
                '    </h3>' +
                '  </div>\n' +
                '  <div class="panel-body">\n' +
                '    <div class="message-block" id="message-block-' + data.success.conversation.id + '">\n' +
                '      <div class="message-item message-item-my">\n' +
                '        <div class="message-heading">\n' +
                '          <b>' + data.success.message.author + '</b>\n' +
                '          <small>' + data.success.message.created_at + '</small>\n' +
                '        </div>\n' +
                '        <div class="message-body">\n' +
                data.success.message.text +
                '        </div>\n' +
                data.attachment_string +
                '      </div>\n' +
                '    </div>\n' +
                '    <textarea id="new-message-' + data.success.conversation.id + '" rows="1" class="form-control" style="border: solid 1px #cccccc"></textarea>\n' +
                '    <button class="btn btn-primary message-submit" id="message-submit-' + data.success.conversation.id + '" ' +
                '       data-conversation_id="' + data.success.conversation.id + '">\n' +
                '      Отправить\n' +
                '    </button>\n' +
                '    <div class="dialog-hover-block float-right conversation-button">\n' +
                '      <span class="message-mention-button" id="message-mention-button-' + data.success.conversation.id + '"\n' +
                '          data-conversation_id="{{$conversation->id}}">\n' +
                '        <i class="fa fa-at" aria-hidden="true"></i>\n' +
                '      </span>\n' +
                '      <ul class="dialog mention-dialog" id="mention-dialog-' + data.success.conversation.id + '">\n' +
                users_string +
                '      </ul>\n' +
                '    </div>' +
                '    <hr/>\n' +
                '    <div class="row">\n' +
                '      <div class="col-md-8">\n' +
                '        Подписчики:\n' +
                '        <span class="followers-list" id="followers-list-' + data.success.conversation.id + '">\n' +
                '        </span>\n' +
                '        <span class="dialog-hover-block">\n' +
                '            <span class="add-follower-button">\n' +
                '                <i class="fa fa-user-plus" aria-hidden="true" title="Добавить подписчика"></i>\n' +
                '            </span>\n' +
                '            <ul class="dialog followers-dialog">\n' +
                followers_string +
                '            </ul>\n' +
                '        </span>' +
                '      </div>\n' +
                '      <div class="col-md-4">\n' +
                '        <span class="follow-button-block float-right"\n' +
                '              data-post_id="' + data.success.conversation.id + '" data-type="conversation">\n' +
                '          <i class="fa fa-bell-o" aria-hidden="true"></i>\n' +
                '          <span class="follow-button-text">\n' +
                '            Подписаться\n' +
                '          </span>\n' +
                '        </span>\n' +
                '      </div>\n' +
                '    </div>' +
                '  </div>\n' +
                '</div>'
            );
        },
        error: function(jqxhr, status, errorMsg) {
            ajaxErrorsHandling(jqxhr, status, errorMsg);
        }
    });
}

function deleteConversationAjax(conversation_id) {
    $('.loading').show();
    ajaxSetup();
    $.ajax({
        url     : "/deleteConversationAjax/",
        dataType: "json",
        data    : {
            conversation_id: conversation_id
        },
        success : function () {
            $('.loading').hide();
            $('#conversations-item-' + conversation_id).detach();
        },
        error: function(jqxhr, status, errorMsg) {
            ajaxErrorsHandling(jqxhr, status, errorMsg);
        }
    });
}

function updateConversationAjax(conversation_id, conversation_title) {
    $('.loading').show();
    ajaxSetup();
    $.ajax({
        url     : "/updateConversationAjax/",
        dataType: "json",
        data    : {
            conversation_id   : conversation_id,
            conversation_title: conversation_title
        },
        success : function () {
            $('.loading').hide();
        },
        error: function(jqxhr, status, errorMsg) {
            ajaxErrorsHandling(jqxhr, status, errorMsg);
        }
    });
}

function sendMessageAjax(message_text, conversation_id) {
    ajaxSetup();
    $.ajax({
        url     : "/sendMessageAjax/",
        dataType: "json",
        data    : {
            message_text   : message_text,
            conversation_id: conversation_id
        },
        success : function () {
            $('#new-message-' + conversation_id).val('');
        },
        error: function(jqxhr, status, errorMsg) {
            ajaxErrorsHandling(jqxhr, status, errorMsg);
        }
    });
}

let conn = new ab.connect(
    'ws://127.0.0.1:8080/ws',
    function (session) {
        session.subscribe('NewMessage', function (topic, data) {
            updateChat(data.data);
        });
    },

    function (code, reason, detail) {
        console.warn('Websocket connection closed! Code = ' +
            code + '; Reason = ' + reason + '; Detail = ' + detail + ';')
    },
    {
        'maxRetries'          : 60,
        'retryDelay'          : 4000,
        'skipSubprotocolCheck': true
    }
);

function updateChat(data) {
    if (data.message_flag == 'new_message') {
        let message_class = '';
        if (data.author_id == $('#current-user').val()) {
            message_class = 'message-item-my';
        } else {
            message_class = 'message-item-other';
        }
        let text_string = '';
        if (data.text) {
            text_string = data.text;
        }
        $('#message-block-' + data.conversation_id).append('' +
            '<div class="message-item ' + message_class + '" id="message-item-' + data.id + '">\n' +
            '  <div class="message-heading">\n' +
            '    <b>' + data.author + '</b> <small>' + data.created_at + '</small>\n' +
            '  </div>\n' +
            '<div class="message-body">\n' +
            text_string +
            '</div>\n' +
            '</div>' +
            '').scrollTop(999999999);
    }
}

function updateFollowerAjax(post_id, user_id, type) {
    $('.loading').show();
    ajaxSetup();
    $.ajax({
        url     : "/updateFollowerAjax/",
        dataType: "json",
        data    : {
            post_id: post_id,
            user_id: user_id,
            type   : type
        },
        success : function (data) {
            $('.loading').hide();
            if (type == 'conversation') {
                if (data.success.followed == 'followed') {
                    if ($('#current-user').val() == user_id) {
                        $('#conversations-item-' + post_id + ' .follow-button-block').addClass('follow-button-block-active');
                        $('#conversations-item-' + post_id + ' .follow-button-text').text(' Вы подписаны');
                    }
                    $('#conversations-item-' + post_id + ' .follower-list-item-' + data.success.user.id + ' .toggle-follower')
                        .removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
                    $('#conversations-item-' + post_id + ' .followers-list').append('' +
                        '<a href="#" class="users-item followers-item-' + data.success.user.id + '">' +
                        data.success.user.name+
                        '</a>');
                } else {
                    if ($('#current-user').val() == user_id) {
                        $('#conversations-item-' + post_id + ' .follow-button-block').removeClass('follow-button-block-active');
                        $('#conversations-item-' + post_id + ' .follow-button-text').text(' Подписаться');
                    }
                    $('#conversations-item-' + post_id + ' .follower-list-item-' + data.success.user.id + ' .toggle-follower')
                        .addClass('fa-plus-square-o').removeClass('fa-minus-square-o');
                    $('#conversations-item-' + post_id + ' .followers-item-' + data.success.user.id).detach();
                }
            }
        },
        error   : function (jqxhr, status, errorMsg) {
            ajaxErrorsHandling(jqxhr, status, errorMsg);
        }
    });
}