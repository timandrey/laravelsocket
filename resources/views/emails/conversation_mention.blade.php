@component('mail::message')
    # Вас упомянули в беседе

    Вас упомянули в беседе "{{$conversation->title}}"

    <a href="{{url('/').'/conversations/'.$conversation->id}}">Посмотреть беседу</a>
@endcomponent