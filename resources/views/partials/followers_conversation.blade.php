<hr/>
<div class="row">
    <div class="col-md-8">
        Подписчики:
        <span class="followers-list">
            @forelse($conversation->followers as $follower)
                <a href="#" class="users-item followers-item-{{$follower->id}}">
                    {{$follower->name}}
                </a>
            @empty
            @endforelse
        </span>
        <span class="dialog-hover-block">
            <span class="add-follower-button">
                <i class="fa fa-user-plus" aria-hidden="true" title="Добавить подписчика"></i>
            </span>
            <ul class="dialog followers-dialog">
                @foreach($conversation->users as $user)
                    @if($user->id != Auth::user()->id)
                        <li data-user_id="{{$user->id}}" data-post_id="{{$conversation->id}}" data-type="conversation"
                            class="follower-list-item-{{$user->id}}">
                            {{$user->name}}
                        </li>
                    @endif
                @endforeach
            </ul>
        </span>
    </div>
    <div class="col-md-4">
        <span class="follow-button-block float-right @if($conversation->is_follower == true) follow-button-block-active @else @endif"
              data-post_id="{{$conversation->id}}" data-type="conversation">
            <i class="fa fa-bell-o" aria-hidden="true"></i>
            <span class="follow-button-text">
                @if($conversation->is_follower == true)
                    <button class="follow-button-block">Отписаться</button>
                    Отписаться
                @else
                    Подписаться
                    <button class="follow-button-block">Подписаться</button>
                @endif
            </span>
        </span>
    </div>
</div>