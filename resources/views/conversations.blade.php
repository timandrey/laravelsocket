@extends('layouts.app')
@section('content')
    {{csrf_field()}}
    <input type="hidden" name="current_user" value="{{Auth::user()->id}}" id="current-user">
    <div class="container-fluid text-center page-header">
        <h1>{{$page_title}}</h1>
    </div>
    <div class="container-fluid page-body">
        <div class="row">
            <div class="col-md-12">
            @if($conversation != null) <!--если мы просматриваем отдельный чат-->
                <div class="panel panel-default conversations-item" id="conversations-item-{{$conversation->id}}">
                    <div class="panel-heading">
                        <h3>
                            <a href="/conversations/{{$conversation->id}}" class="{{$conversation->title_class}}"
                               id="conversation-title-{{$conversation->id}}">
                                {{$conversation->title}}
                            </a>
                            <input type="text" style="display: none" id="conversation-title-input-{{$conversation->id}}"
                                   class="conversation-title-input" value="{{$conversation->title}}" data-id="{{$conversation->id}}">
                            <div class="float-right conversation-header-button conversation-delete-button"
                                 data-id="{{$conversation->id}}" data-single="yes">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </div>
                            <div class="float-right conversation-header-button conversation-edit-title-button"
                                 data-id="{{$conversation->id}}">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </div>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="message-block" id="message-block-{{$conversation->id}}">
                            @foreach($conversation->messages as $message)
                                <div class="message-item {{$message->class}}">
                                    <div class="message-body">
                                        {!! $message->text !!}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        @include('partials.create_message')
                        @include('partials.followers_conversation')
                    </div>
                </div>
            @else <!--если просматриваем список чатов - отобразим форму создания нового чата и список всех чатов-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Новое обсуждение</h3>
                    </div>
                    <div class="panel-body">
                        @include('partials.create_conversation')
                    </div>
                </div>
                <div class="conversations-block">
                    @forelse($conversations as $conversation)
                        <div class="panel panel-default conversations-item" id="conversations-item-{{$conversation->id}}">
                            <div class="panel-heading">
                                <h3>
                                    <a href="/conversations/{{$conversation->id}}" class="{{$conversation->title_class}}"
                                       id="conversation-title-{{$conversation->id}}">
                                        {{$conversation->title}}
                                    </a>
                                    <input type="text" style="display: none" id="conversation-title-input-{{$conversation->id}}"
                                           class="conversation-title-input" value="{{$conversation->title}}"
                                           data-id="{{$conversation->id}}">
                                    <div class="float-right conversation-header-button conversation-delete-button"
                                         data-id="{{$conversation->id}}">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="float-right conversation-header-button conversation-edit-title-button"
                                         data-id="{{$conversation->id}}">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </div>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div class="message-block" id="message-block-{{$conversation->id}}">
                                    @foreach($conversation->messages as $message)
                                        <div class="message-item">
                                            <div class="message-body">
                                                {{ $message->text }}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                @include('partials.create_message')
                            </div>
                        </div>
                    @empty
                    @endforelse
                </div>
                @endif
            </div>
        </div>
    </div>
    <script src="{{ asset('js/autobahn.min.js') }}"></script>
    <script src="{{asset('js/conversations.js')}}"></script>
@endsection