<?php

namespace App\Http\Controllers;

use App\Model\Conversation;
use App\Model\Follower;
use App\Model\Message;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConversationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($conversation_id = 0)
    {
        $current_user = Auth::user();
        $page_title   = 'Обсуждения';
        $users        = User::all();
        $conversations = Conversation::where('is_visible', 1)->orderBy('updated_at', 'desc')->get();

        if ($conversation = Conversation::find($conversation_id)) {
            $conversation = Conversation::getConversationInfo($conversation);
            if ($conversation->title) {
                $page_title = 'Обсуждение "' . $conversation->title . '"';
            } else {
                $page_title = 'Обсуждение без темы';
            }
        } else {
            $conversation = null;
        }
        return view(
            'conversations',
            [
                'conversation'      => $conversation,
                'conversations'     => $conversations,
                'current_user'      => $current_user,
                'page_title'        => $page_title,
                'users'             => $users
            ]
        );
    }

    public function create(Request $request)
    {
        $conversation           = new Conversation;
        $conversation->title    = $request->conversation_title;
        $conversation->owner_id = $request->user_id;
        $conversation->save();
        $conversation = Conversation::getConversationInfo($conversation);

        $message                  = new Message;
        $message->text            = $request->message_text;
        $message->author_id       = $request->user_id;
        $message->conversation_id = $conversation->id;
        $message->save();
        $message->author = User::find($message->author_id)->name;

        $result['conversation'] = $conversation;
        $result['message']      = $message;
        $result['users']        = User::all();
        $this->sendResponse($result);
    }

    public function delete(Request $request)
    {
        $conversation             = Conversation::find($request->conversation_id);
        $conversation->is_visible = 0;
        $conversation->save();
        $this->sendResponse($conversation);
    }

    public function update(Request $request)
    {
        $conversation        = Conversation::find($request->conversation_id);
        $conversation->title = $request->conversation_title;
        $conversation->save();
        $this->sendResponse($conversation);
    }

    public function sendMessageAjax(Request $request)
    {
        $message                  = new Message;
        $message->text            = $request->message_text;
        $message->author_id       = Auth::user()->id;
        $message->conversation_id = $request->conversation_id;
        $message->save();
        $message->author       = Auth::user()->name;
        $message->conversation = Conversation::find($message->conversation_id);
        $message->message_flag = 'new_message';

        $data = [
            'topic_id' => 'NewMessage',
            'data'     => $message
        ];


        $this->publishMessageInRabbit($data);
        $this->sendResponse($message);
    }

    public function updateFollowerAjax(Request $request)
    {
        if (Follower::isFollower($request->user_id, $request->type, $request->post_id)) {
            $follower = Follower::getFollower($request->user_id, $request->post_id);
            $follower->delete();
            $result['followed'] = 'unfollowed';
        } else {
            $follower              = new Follower();
            $follower->type        = $request->type;
            $follower->follower_id = $request->user_id;
            $follower->post_id     = $request->post_id;
            $follower->save();
            $result['followed'] = 'followed';
        }
        $user           = User::find($request->user_id);
        $result['user'] = $user;
        $this->sendResponse($result);
    }

    public function sendResponse($data) {
        if ($data) {
            echo json_encode(['success' => $data]);
        } else {
            echo json_encode(['fail' => 'Error occurred']);
        }
    }

    public function publishMessageInRabbit($data) {
        $bunny = new \Bunny\Client([
            "host"     => $_ENV['RABBITMQ_HOST'],
            "port"     => $_ENV['RABBITMQ_PORT'],
            "vhost"    => $_ENV['RABBITMQ_VHOST'],
            "user"     => $_ENV['RABBITMQ_USERNAME'],
            "password" => $_ENV['RABBITMQ_PASSWORD'],
        ]);
        $bunny->connect();

        $channel = $bunny->channel();
        $channel->publish(
            json_encode($data),
            [],
            '',
            'updatemessage'
        );
    }
}
