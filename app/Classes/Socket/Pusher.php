<?php


namespace App\Classes\Socket;

use App\Classes\Socket\Base\BasePusher;
use ZMQContext;

class Pusher extends BasePusher
{
    static function sendDataToServer($data)
    {
        $context = new ZMQContext;
        $socket  = $context->getSocket(\ZMQ::SOCKET_PUSH, 'my_pusher');

        $socket->connect('tcp://127.0.0.1:5555');
        //$data = json_encode($data);
        $socket->send($data);
    }

    public function broadcast($json_data_to_send)
    {
        $data_to_send      = json_decode($json_data_to_send);
        $subscribed_topics = $this->getSubcribedTopics();
        if(is_object($data_to_send)){
            if (isset($subscribed_topics[$data_to_send->topic_id])) {
                $topic = $subscribed_topics[$data_to_send->topic_id];
                $topic->broadcast($data_to_send);
            }
        }
    }
}