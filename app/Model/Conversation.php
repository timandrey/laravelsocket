<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Conversation extends Model
{
    public static function getConversationInfo($conversation)
    {
        if ($conversation->title) {
            $conversation->title_class = 'with-title';
        } else {
            $conversation->title_class = 'without-title';
            $conversation->title       = 'Без темы';
        }
        $conversation->messages    = Message::where('conversation_id', $conversation->id)->get();
        $conversation->is_follower = Follower::isFollower(Auth::user()->id, 'conversation', $conversation->id);
        $conversation->users       = User::all();
        return $conversation;
    }
}
