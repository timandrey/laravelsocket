<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{

    public function users() {
        return $this->hasMany(User::class);
    }

    public static function getFollower($user_id, $post_id)
    {
        return Follower::where('follower_id', $user_id)->where('post_id', $post_id);
    }

    public static function isFollower($user_id, $type, $post_id)
    {
        if (Follower::where('follower_id', $user_id)
            ->where('type', $type)
            ->where('post_id', $post_id)
            ->first()) {
            return true;
        } else {
            return false;
        }
    }
}
